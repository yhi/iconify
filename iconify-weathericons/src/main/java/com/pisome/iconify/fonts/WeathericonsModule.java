package com.pisome.iconify.fonts;

import com.pisome.iconify.Icon;
import com.pisome.iconify.IconFontDescriptor;

public class WeathericonsModule implements IconFontDescriptor {

    @Override
    public String ttfFileName() {
        return "iconify/android-iconify-weathericons.ttf";
    }

    @Override
    public Icon[] characters() {
        return WeathericonsIcons.values();
    }

    @Override
    public String prefix()
    {
        return "wi";
    }
}
