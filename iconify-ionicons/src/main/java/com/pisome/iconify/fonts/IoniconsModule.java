package com.pisome.iconify.fonts;

import com.pisome.iconify.Icon;
import com.pisome.iconify.IconFontDescriptor;

public class IoniconsModule implements IconFontDescriptor {

    @Override
    public String ttfFileName() {
        return "iconify/android-iconify-ionicons.ttf";
    }

    @Override
    public Icon[] characters() {
        return IoniconsIcons.values();
    }

    @Override
    public String prefix()
    {
        return "ion";
    }
}
