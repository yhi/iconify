package com.pisome.iconify.sample;

import android.app.Application;

import com.mikepenz.iconics.Iconics;
import com.pisome.iconify.Iconify;

public class SampleApplication extends Application
{
  @Override
  public void onCreate()
  {
    super.onCreate();
    Iconify.init(this.getApplicationContext());
    for (Font font : Font.values()) { Iconify.with(font.getFont()); }
    Iconics.init(this.getApplicationContext());
  }
}
