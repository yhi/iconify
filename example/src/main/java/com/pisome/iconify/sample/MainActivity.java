package com.pisome.iconify.sample;

import android.os.Bundle;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.pisome.iconify.IconDrawable;
import com.pisome.iconify.IconifyLayoutInflaterFactory;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity
{
//  @BindView(R.id.tv)
//  TextView tv;
  @BindView(R.id.iv1)
  ImageView iv1;
  @BindView(R.id.iv2)
  ImageView iv2;
  @BindView(R.id.btn)
  Button btn;
//  @BindView(R.id.toolbar)
//  Toolbar toolbar;
//  @BindView(R.id.viewPager)
//  ViewPager viewPager;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    LayoutInflaterCompat.setFactory(this.getLayoutInflater(), new IconifyLayoutInflaterFactory(this.getDelegate()));
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);
    IconDrawable icon = new IconDrawable(this, "fa-facebook").sizeDpX(32).sizeDpY(128).colorRes(R.color.color_accent).respectFontBounds(false).contourColor(0xFFFF0000).contourWidthDp(1);
    this.iv1.setImageDrawable(icon);
    IconicsDrawable icon2 = new IconicsDrawable(this).icon(FontAwesome.Icon.faw_facebook).sizeDpX(32).sizeDpY(128).colorRes(R.color.color_accent).respectFontBounds(false).contourColor(0xFFFF0000).contourWidthDp(1);
    this.iv2.setImageDrawable(icon2);
    // Setup toolbar
//    setSupportActionBar(toolbar);

    // Fill view pager
//    viewPager.setAdapter(new FontIconsViewPagerAdapter(Font.values()));
//    tabLayout.setupWithViewPager(viewPager);
  }

  @OnClick(R.id.btn)
  void onClick(View v)
  {
    this.btn.setText(this.getResources().getString(R.string.alert) + "{fa-twitter}");
  }
}
