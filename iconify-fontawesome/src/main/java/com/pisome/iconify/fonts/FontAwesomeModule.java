package com.pisome.iconify.fonts;

import com.pisome.iconify.Icon;
import com.pisome.iconify.IconFontDescriptor;

public class FontAwesomeModule implements IconFontDescriptor {

    @Override
    public String ttfFileName() {
        return "iconify/android-iconify-fontawesome.ttf";
    }

    @Override
    public Icon[] characters() {
        return FontAwesomeIcons.values();
    }

    @Override
    public String prefix()
    {
        return "fa-";
    }
}
