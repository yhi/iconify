package com.pisome.iconify.fonts;

import com.pisome.iconify.Icon;
import com.pisome.iconify.IconFontDescriptor;

public class TypiconsModule implements IconFontDescriptor {

    @Override
    public String ttfFileName() {
        return "iconify/android-iconify-typicons.ttf";
    }

    @Override
    public Icon[] characters() {
        return TypiconsIcons.values();
    }

    @Override
    public String prefix()
    {
        return "typcn";
    }
}
