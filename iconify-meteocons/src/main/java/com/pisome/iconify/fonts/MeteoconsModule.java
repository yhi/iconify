package com.pisome.iconify.fonts;

import com.pisome.iconify.Icon;
import com.pisome.iconify.IconFontDescriptor;

public class MeteoconsModule implements IconFontDescriptor {

    @Override
    public String ttfFileName() {
        return "iconify/android-iconify-meteocons.ttf";
    }

    @Override
    public Icon[] characters() {
        return MeteoconsIcons.values();
    }

    @Override
    public String prefix()
    {
        return "mc";
    }
}
