package com.pisome.iconify.fonts;

import com.pisome.iconify.Icon;
import com.pisome.iconify.IconFontDescriptor;

public class EntypoModule implements IconFontDescriptor {

    @Override
    public String ttfFileName() {
        return "iconify/android-iconify-entypo.ttf";
    }

    @Override
    public Icon[] characters() {
        return EntypoIcons.values();
    }

    @Override
    public String prefix()
    {
        return "entypo";
    }
}
