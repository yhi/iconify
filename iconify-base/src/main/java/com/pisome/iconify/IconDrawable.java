package com.pisome.iconify;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.Dimension;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.text.TextPaint;
import android.text.TextUtils;

import com.pisome.iconify.internal.IconFontDescriptorWrapper;
import com.pisome.tool.Screens;

import java.util.HashMap;
import java.util.Map;

import static android.support.annotation.Dimension.DP;
import static android.support.annotation.Dimension.PX;

/**
 * Embed an icon into a Drawable that can be used as TextView icons, or ActionBar icons.
 * <pre>
 *     new IconDrawable(context, IconValue.icon_star)
 *           .colorRes(R.color.white)
 *           .actionBarSize();
 * </pre>
 * If you don't set the size of the drawable, it will use the size
 * that is given to him. Note that in an ActionBar, if you don't
 * set the size explicitly it uses 0, so please use actionBarSize().
 */
public class IconDrawable extends Drawable
{

  public static final int ANDROID_ACTIONBAR_ICON_SIZE_DP = 24;

  private Context context;

  private Icon icon;
  private int alpha;
  private int sizeX = -1;
  private int sizeY = -1;
  private int padding;
  private int contourWidth;
  private int offsetX = 0;
  private int offsetY = 0;
  private boolean drawContour;
  private boolean respectFontBounds = false;

  private Typeface typeface;

  private Paint iconPaint;
  private Paint contourPaint;
  private Rect paddingBounds;
  private Path path;
  private RectF pathBounds;

  /**
   * Create an IconDrawable.
   *
   * @param context  Your activity or application context.
   * @param iconName The icon key you want this drawable to display.
   * @throws IllegalArgumentException if the key doesn't match any icon.
   */
  public IconDrawable(Context context, String iconName)
  {
    this(context, Iconify.findIconForKey(iconName));
  }

  public IconDrawable(Context context, Icon icon)
  {
    this.context = context.getApplicationContext();
    init(icon);
  }

  public IconDrawable icon(String iconName)
  {
    return this.icon(Iconify.findIconForKey(iconName));
  }

  public IconDrawable icon(Icon icon)
  {
    this.init(icon);
    this.invalidateSelf();
    return this;
  }

  private void init(Icon icon)
  {
    if (icon != null)
    {
      IconFontDescriptorWrapper descriptor = Iconify.findTypefaceOf(icon);
      if (descriptor != null)
      {
        this.typeface = descriptor.getTypeface(this.context);
      }
    }

    this.icon = icon;
    this.iconPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
    this.iconPaint.setTypeface(this.typeface);
    this.iconPaint.setStyle(Paint.Style.FILL);
    this.iconPaint.setTextAlign(Paint.Align.CENTER);
    this.iconPaint.setUnderlineText(false);
    this.iconPaint.setColor(Color.BLACK);
    this.iconPaint.setAntiAlias(true);

    this.contourPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    this.contourPaint.setStyle(Paint.Style.STROKE);

    this.path = new Path();
    this.pathBounds = new RectF();
    this.paddingBounds = new Rect();
  }

  public static IconDrawable create(final Context ctx, final TypedArray ta)
  {
    IconDrawable icon = null;

    String iconName = ta.getString(R.styleable.Iconify_icon_name);

    if (!TextUtils.isEmpty(iconName))
    {
      icon = new IconDrawable(ctx, iconName);

      Map<String, Integer> iconAttrs = new HashMap<>();
      iconAttrs.put("iconColor", ta.getColor(R.styleable.Iconify_icon_color, 0));
      iconAttrs.put("iconSizeX", ta.getDimensionPixelSize(R.styleable.Iconify_icon_size_x, -1));
      iconAttrs.put("iconSizeY", ta.getDimensionPixelSize(R.styleable.Iconify_icon_size_x, -1));
      iconAttrs.put("iconOffsetX", ta.getDimensionPixelSize(R.styleable.Iconify_icon_offset_x, -1));
      iconAttrs.put("iconOffsetY", ta.getDimensionPixelSize(R.styleable.Iconify_icon_offset_x, -1));
      iconAttrs.put("iconPadding", ta.getDimensionPixelSize(R.styleable.Iconify_icon_padding, -1));
      iconAttrs.put("iconContourColor", ta.getColor(R.styleable.Iconify_icon_contour_color, 0));
      iconAttrs.put("iconContourWidth", ta.getDimensionPixelSize(R.styleable.Iconify_icon_contour_width, -1));
      icon.setAttrs(iconAttrs);
    }

    return icon;
  }

  private void setAttrs(Map<String, Integer> attrs)
  {
    this.color(attrs.get("iconColor"));
    this.sizeX(attrs.get("iconSizeX"));
    this.sizeY(attrs.get("iconSizeY"));
    this.offsetX(attrs.get("iconOffsetX"));
    this.offsetY(attrs.get("iconOffsetY"));
    this.padding(attrs.get("iconPadding"));
    this.contourColor(attrs.get("iconContourColor"));
    this.contourWidth(attrs.get("iconContourWidth"));
  }

  private Pair<Integer, Integer> discomposeColor(@ColorInt int argb)
  {
    int alpha = Color.alpha(argb);
    int r = Color.red(argb);
    int g = Color.green(argb);
    int b = Color.blue(argb);
    int color = Color.rgb(r, g, b);
    return new Pair<>(alpha, color);
  }

  public IconDrawable colorRes(@ColorRes int colorRes)
  {
    return this.color(ContextCompat.getColor(this.context, colorRes));
  }

  public IconDrawable color(@ColorInt int iconColor)
  {
    Pair<Integer, Integer> color = this.discomposeColor(iconColor);
    this.setAlpha(color.first);
    this.iconPaint.setColor(color.second);
    invalidateSelf();
    return this;
  }

  public IconDrawable contourColor(@ColorInt int contourColor)
  {
    Pair<Integer, Integer> color = this.discomposeColor(contourColor);
    this.contourPaint.setAlpha(color.first);
    this.contourPaint.setColor(color.second);
    invalidateSelf();
    return this;
  }

  public IconDrawable alpha(int alpha)
  {
    this.setAlpha(alpha);
    invalidateSelf();
    return this;
  }

  public IconDrawable respectFontBounds(boolean respectBounds)
  {
    this.respectFontBounds = respectBounds;
    invalidateSelf();
    return this;
  }

  public IconDrawable sizeRes(@DimenRes int dimenRes)
  {
    return size(this.context.getResources().getDimensionPixelSize(dimenRes));
  }

  public IconDrawable size(@Dimension(unit = PX) int size)
  {
    this.sizeX = this.sizeY = size;
    this.setBounds(0, 0, size, size);
    invalidateSelf();
    return this;
  }

  public IconDrawable sizeResX(@DimenRes int dimenResX)
  {
    return sizeX(context.getResources().getDimensionPixelSize(dimenResX));
  }

  public IconDrawable sizeX(@Dimension(unit = PX) int sizeX)
  {
    this.sizeX = sizeX;
    setBounds(0, 0, sizeX, sizeY);
    invalidateSelf();
    return this;
  }

  public IconDrawable sizeDpX(@Dimension(unit = DP) int sizeX)
  {
    return sizeX(Screens.dp2px(sizeX));
  }

  public IconDrawable sizeResY(@DimenRes int dimenResY)
  {
    return sizeY(this.context.getResources().getDimensionPixelSize(dimenResY));
  }

  public IconDrawable sizeY(@Dimension(unit = PX) int sizeY)
  {
    this.sizeY = sizeY;
    this.setBounds(0, 0, this.sizeX, this.sizeY);
    this.invalidateSelf();
    return this;
  }

  public IconDrawable sizeDpY(@Dimension(unit = DP) int sizeY)
  {
    return this.sizeY(Screens.dp2px(sizeY));
  }

  public IconDrawable paddingDp(@Dimension(unit = DP) int iconPadding)
  {
    return this.padding(Screens.dp2px(iconPadding));
  }

  public IconDrawable padding(@Dimension(unit = PX) int iconPadding)
  {
    if (this.padding != iconPadding)
    {
      this.padding = iconPadding;
      if (this.drawContour)
      {
        this.padding += this.contourWidth;
      }
      this.invalidateSelf();
    }
    return this;
  }

  public IconDrawable paddingRes(@DimenRes int dimenRes)
  {
    return padding(this.context.getResources().getDimensionPixelSize(dimenRes));
  }

  public IconDrawable contourWidthRes(@DimenRes int contourWidthRes)
  {
    return contourWidth(this.context.getResources().getDimensionPixelSize(contourWidthRes));
  }

  public IconDrawable contourWidth(@Dimension(unit = PX) int contourWidth)
  {
    this.contourWidth = contourWidth;
    this.contourPaint.setStrokeWidth(this.contourWidth);
    this.drawContour(true);
    this.invalidateSelf();
    return this;
  }

  private IconDrawable drawContour(boolean drawContour)
  {
    if (this.drawContour != drawContour)
    {
      this.drawContour = drawContour;

      if (this.drawContour)
      {
        this.padding += this.contourWidth;
      }
      else
      {
        this.padding -= this.contourWidth;
      }
      this.invalidateSelf();
    }
    return this;
  }

  public IconDrawable contourWidthDp(@Dimension(unit = DP) int contourWidthDp)
  {
    return contourWidth(Screens.dp2px(contourWidthDp));
  }

  public IconDrawable actionBarSize()
  {
    return sizeDp(ANDROID_ACTIONBAR_ICON_SIZE_DP);
  }

  public IconDrawable sizeDp(@Dimension(unit = DP) int size)
  {
    return size(Screens.dp2px(size));
  }

  /**
   * set the icon offset for X from resource
   *
   * @param iconOffsetXRes
   * @return
   */
  public IconDrawable offsetXRes(@DimenRes int iconOffsetXRes)
  {
    return offsetX(this.context.getResources().getDimensionPixelSize(iconOffsetXRes));
  }

  /**
   * set the icon offset for X
   *
   * @param iconOffsetX
   * @return
   */
  public IconDrawable offsetX(@Dimension(unit = PX) int iconOffsetX)
  {
    this.offsetX = iconOffsetX;
    return this;
  }

  /**
   * set the icon offset for X as dp
   *
   * @param iconOffsetXDp
   * @return
   */
  public IconDrawable offsetXDp(@Dimension(unit = DP) int iconOffsetXDp)
  {
    return offsetX(Screens.dp2px(iconOffsetXDp));
  }

  /**
   * set the icon offset for Y from resource
   *
   * @param iconOffsetYRes
   * @return
   */
  public IconDrawable offsetYRes(@DimenRes int iconOffsetYRes)
  {
    return offsetY(this.context.getResources().getDimensionPixelSize(iconOffsetYRes));
  }

  /**
   * set the icon offset for Y
   *
   * @param iconOffsetY
   * @return
   */
  public IconDrawable offsetY(@Dimension(unit = PX) int iconOffsetY)
  {
    this.offsetY = iconOffsetY;
    return this;
  }

  /**
   * set the icon offset for Y as dp
   *
   * @param iconOffsetYDp
   * @return
   */
  public IconDrawable offsetYDp(@Dimension(unit = DP) int iconOffsetYDp)
  {
    return offsetY(Screens.dp2px(iconOffsetYDp));
  }

  @Override
  public void draw(Canvas canvas)
  {
    if (this.icon == null) { return; }

    Rect bounds = this.getBounds();

    if (this.padding >= 0 && !(this.padding * 2 > bounds.width()) && !(this.padding * 2 > bounds.height()))
    {
      this.paddingBounds.set(bounds.left + this.padding, bounds.top + this.padding, bounds.right - this.padding, bounds.bottom - this.padding);
    }

    float textSize = bounds.height() * (this.respectFontBounds ? 1 : 2);
    String textValue = String.valueOf(icon.character());
    this.iconPaint.setTextSize(textSize);
    this.iconPaint.getTextPath(textValue, 0, textValue.length(), 0, bounds.height(), this.path);
    this.path.computeBounds(this.pathBounds, true);
    if (!this.respectFontBounds)
    {
      float deltaWidth = ((float) this.paddingBounds.width() / this.pathBounds.width());
      float deltaHeight = ((float) this.paddingBounds.height() / this.pathBounds.height());
      float delta = (deltaWidth < deltaHeight) ? deltaWidth : deltaHeight;
      textSize *= delta;
      this.iconPaint.setTextSize(textSize);
      this.iconPaint.getTextPath(textValue, 0, textValue.length(), 0, bounds.height(), this.path);
      this.path.computeBounds(this.pathBounds, true);
    }

    float x = bounds.centerX() - (this.pathBounds.width() / 2) - this.pathBounds.left;
    float y = bounds.centerY() - (this.pathBounds.height() / 2) - this.pathBounds.top;
    this.path.offset(x + this.offsetX, y + this.offsetY);
    this.path.close();
    this.iconPaint.setAlpha(this.alpha);
    if (drawContour) { canvas.drawPath(this.path, this.contourPaint); }
    canvas.drawPath(this.path, this.iconPaint);
  }

  @Override
  public void setAlpha(int alpha)
  {
    this.alpha = alpha;
    this.iconPaint.setAlpha(alpha);
  }

  @Override
  public void setColorFilter(ColorFilter cf)
  {
    this.iconPaint.setColorFilter(cf);
  }

  @Override
  public void clearColorFilter()
  {
    this.iconPaint.setColorFilter(null);
  }

  @Override
  public boolean isStateful()
  {
    return true;
  }

//  @Override
//  public boolean setState(int[] stateSet)
//  {
//    int oldValue = this.iconPaint.getAlpha();
//    int newValue = isEnabled(stateSet) ? alpha : alpha / 2;
//    this.iconPaint.setAlpha(newValue);
//    return oldValue != newValue;
//  }

  @Override
  public int getOpacity()
  {
    return this.alpha;
  }

  @Override
  public int getIntrinsicWidth()
  {
    return sizeX;
  }

  @Override
  public int getIntrinsicHeight()
  {
    return sizeY;
  }

  // Util
  private boolean isEnabled(int[] stateSet)
  {
    for (int state : stateSet)
      if (state == android.R.attr.state_enabled)
        return true;
    return false;
  }

  public void setStyle(Paint.Style style)
  {
    this.iconPaint.setStyle(style);
  }
}