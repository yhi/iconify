package com.pisome.iconify;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.view.LayoutInflaterFactory;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.view.menu.ActionMenuItemView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import static com.pisome.iconify.Iconify.compute;

public final class IconifyLayoutInflaterFactory implements LayoutInflaterFactory
{
  private static final String TAG = "ILIF";
  private final AppCompatDelegate delegate;

  public IconifyLayoutInflaterFactory(final AppCompatDelegate delegate)
  {
    this.delegate = delegate;
  }

  @Override
  public View onCreateView(final View parent, final String name, final Context context, final AttributeSet attrs)
  {
    View view = this.delegate.createView(parent, name, context, attrs);

    if (view == null) { return view; }

    if (view instanceof ActionMenuItemView)
    {
      final ActionMenuItemView amiv = (ActionMenuItemView)view;
      final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.Iconify);
      IconDrawable icon = IconDrawable.create(context, a);
      amiv.setIcon(icon);
      a.recycle();
    }
    else if (view instanceof EditText)
    {
      final EditText et = (EditText)view;
      CharSequence text = compute(context, et.getText(), et);
      CharSequence hint = compute(context, et.getHint(), et);
      et.setAllCaps(false);
      et.setText(text);
      et.setHint(hint);
    }
    else if (view instanceof TextView)
    {
      final TextView tv = (TextView) view;
      CharSequence result = compute(context, tv.getText(), tv);

      tv.setAllCaps(false);
      tv.setText(result);

      tv.addTextChangedListener(new TextWatcher()
      {
        @Override
        public void beforeTextChanged(final CharSequence charSequence, final int i, final int i1, final int i2)
        {
        }

        @Override
        public void onTextChanged(final CharSequence before, final int i, final int i1, final int i2)
        {
        }

        @Override
        public void afterTextChanged(Editable before)
        {
          Iconify.compute(context, before);
        }
      });
    }
    else if (view instanceof ImageView)
    {
      ImageView iv = (ImageView) view;
      final TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.Iconify);
      IconDrawable icon = IconDrawable.create(context, ta);
      iv.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
      iv.setImageDrawable(icon);
      ta.recycle();
    }
    return view;
  }
}
