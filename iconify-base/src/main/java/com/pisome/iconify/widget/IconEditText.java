package com.pisome.iconify.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.EditText;

import com.pisome.iconify.Iconify;
import com.pisome.iconify.internal.HasOnViewAttachListener;

public class IconEditText extends EditText implements HasOnViewAttachListener
{
  private HasOnViewAttachListener.HasOnViewAttachListenerDelegate delegate;

  public IconEditText(final Context context)
  {
    this(context, null);
  }

  public IconEditText(final Context context, final AttributeSet attrs)
  {
    this(context, attrs, android.R.attr.editTextStyle);
  }

  public IconEditText(final Context context, final AttributeSet attrs, final int defStyleAttr)
  {
    super(context, attrs, defStyleAttr);
  }

  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  public IconEditText(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes)
  {
    super(context, attrs, defStyleAttr, defStyleRes);
  }

  @Override
  public void setText(CharSequence text, BufferType type)
  {
    super.setText(Iconify.compute(getContext(), text, this), type);
  }

  @Override
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    delegate.onAttachedToWindow();
  }

  @Override
  public void setOnViewAttachListener(HasOnViewAttachListener.OnViewAttachListener listener)
  {
    if (delegate == null) delegate = new HasOnViewAttachListener.HasOnViewAttachListenerDelegate(this);
    delegate.setOnViewAttachListener(listener);
  }

  @Override
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    delegate.onDetachedFromWindow();
  }
}
