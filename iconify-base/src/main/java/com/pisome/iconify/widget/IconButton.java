package com.pisome.iconify.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.Button;

import com.pisome.iconify.Iconify;
import com.pisome.iconify.internal.HasOnViewAttachListener;

public class IconButton extends Button implements HasOnViewAttachListener
{
//  private float radius = 0.0F;
  private HasOnViewAttachListenerDelegate delegate;

  public IconButton(Context context)
  {
    this(context, null);
  }

  public IconButton(Context context, AttributeSet attrs)
  {
    this(context, attrs, android.R.attr.buttonStyle);
  }

  public IconButton(Context context, AttributeSet attrs, int defStyle)
  {
    super(context, attrs, defStyle);
//
//    if (!this.isInEditMode())
//    {
//      TypedArray attrsArray = context.obtainStyledAttributes(attrs, R.styleable.Iconify, 0, 0);
//      this.radius = attrsArray.getDimension(R.styleable.Iconify_radius, radius);
//      attrsArray.recycle();
//      this.init();
//    }
  }

  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  public IconButton(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes)
  {
    super(context, attrs, defStyleAttr, defStyleRes);
  }

//  private void init()
//  {
//    setTransformationMethod(null);
//    this.setUpBackground();
//  }

//  private void setUpBackground()
//  {
//    Drawable currentBackground = this.getBackground();
//
//    if (currentBackground instanceof ColorDrawable)
//    {
//      if (this.radius != 0.0F)
//      {
//        GradientDrawable background = new GradientDrawable();
//        background.setCornerRadius(this.radius);
//        ColorDrawable cd = (ColorDrawable) currentBackground;
//        background.setIconColor(cd.getColor());
//        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN)
//        {
//          this.setBackgroundDrawable(background);
//        }
//        else
//        {
//          this.setBackground(background);
//        }
//      }
//    }
//  }

  @Override
  public void setText(CharSequence text, BufferType type)
  {
    super.setText(Iconify.compute(getContext(), text, this), type);
  }

  @Override
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    delegate.onAttachedToWindow();
  }

  @Override
  public void setOnViewAttachListener(HasOnViewAttachListener.OnViewAttachListener listener)
  {
    if (delegate == null) delegate = new HasOnViewAttachListenerDelegate(this);
    delegate.setOnViewAttachListener(listener);
  }

  @Override
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    delegate.onDetachedFromWindow();
  }
}
