package com.pisome.iconify.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ToggleButton;

import com.pisome.iconify.Iconify;
import com.pisome.iconify.internal.HasOnViewAttachListener;

public class IconToggleButton extends ToggleButton implements HasOnViewAttachListener
{

  private HasOnViewAttachListenerDelegate delegate;

  public IconToggleButton(Context context, AttributeSet attrs, int defStyle)
  {
    super(context, attrs, defStyle);
//    init();
  }

//  private void init()
//  {
//    setTransformationMethod(null);
//  }

  public IconToggleButton(Context context, AttributeSet attrs)
  {
    super(context, attrs, android.R.attr.buttonStyleToggle);
//    init();
  }

  public IconToggleButton(Context context)
  {
    this(context, null);
//    init();
  }

  @Override
  public void setText(CharSequence text, BufferType type)
  {
    super.setText(Iconify.compute(getContext(), text, this), BufferType.NORMAL);
  }

  @Override
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    delegate.onAttachedToWindow();
  }

  @Override
  public void setOnViewAttachListener(HasOnViewAttachListener.OnViewAttachListener listener)
  {
    if (delegate == null) delegate = new HasOnViewAttachListenerDelegate(this);
    delegate.setOnViewAttachListener(listener);
  }

  @Override
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    delegate.onDetachedFromWindow();
  }

}
