package com.pisome.iconify.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;

import com.pisome.iconify.internal.HasOnViewAttachListener;

public class IconTabLayout extends TabLayout implements HasOnViewAttachListener
{
//  private int tabTextSize;
  private ColorStateList tabTextColors;

  private HasOnViewAttachListenerDelegate delegate;
  private Context ctx;

  public IconTabLayout(final Context context)
  {
    this(context, null, 0);
  }

  public IconTabLayout(final Context context, final AttributeSet attrs)
  {
    this(context, attrs, 0);
  }

  public IconTabLayout(Context context, AttributeSet attrs, int defStyleAttr)
  {
    super(context, attrs, defStyleAttr);
    this.ctx = context;
    TypedArray a = context.obtainStyledAttributes(attrs, android.support.design.R.styleable.TabLayout,
    defStyleAttr, android.support.design.R.style.Widget_Design_TabLayout);

    int tabTextAppearance = a.getResourceId(android.support.design.R.styleable.TabLayout_tabTextAppearance,
    android.support.design.R.style.TextAppearance_Design_Tab);

    // Text colors/sizes come from the text appearance first
    final TypedArray ta = context.obtainStyledAttributes(tabTextAppearance, android.support.design.R.styleable.TextAppearance);
    try
    {
//      this.tabTextSize = ta.getDimensionPixelSize(android.support.design.R.styleable.TextAppearance_android_textSize, 0);
      this.tabTextColors = ta.getColorStateList(android.support.design.R.styleable.TextAppearance_android_textColor);
    }
    finally
    {
      ta.recycle();
    }

    if (a.hasValue(android.support.design.R.styleable.TabLayout_tabTextColor))
    {
      // If we have an explicit text color set, use it instead
      this.tabTextColors = a.getColorStateList(android.support.design.R.styleable.TabLayout_tabTextColor);
    }

    if (a.hasValue(android.support.design.R.styleable.TabLayout_tabSelectedTextColor))
    {
      // We have an explicit selected text color set, so we need to make merge it with the
      // current colors. This is exposed so that developers can use theme attributes to set
      // this (theme attrs in ColorStateLists are Lollipop+)
      final int selected = a.getColor(android.support.design.R.styleable.TabLayout_tabSelectedTextColor, 0);
      this.tabTextColors = this.createColorStateList(this.tabTextColors.getDefaultColor(), selected);
    }
    a.recycle();
  }

  private static ColorStateList createColorStateList(int defaultColor, int selectedColor)
  {
    final int[][] states = new int[2][];
    final int[] colors = new int[2];
    int i = 0;

    states[i] = SELECTED_STATE_SET;
    colors[i] = selectedColor;
    i++;

    // Default enabled state
    states[i] = EMPTY_STATE_SET;
    colors[i] = defaultColor;
    i++;

    return new ColorStateList(states, colors);
  }

  @Override
  public void setupWithViewPager(@Nullable final ViewPager viewPager)
  {
    super.setupWithViewPager(viewPager);
    PagerAdapter pa = viewPager.getAdapter();
    for (int i = 0; i < pa.getCount(); i++)
    {
      IconTextView t = new IconTextView(this.ctx, null);
      t.setText(pa.getPageTitle(i));
      t.setGravity(GRAVITY_CENTER);
      t.setTextColor(this.tabTextColors);
      this.getTabAt(i).setCustomView(t);
    }
  }

//  @Override
//  protected void onAttachedToWindow()
//  {
//    super.onAttachedToWindow();
//    delegate.onAttachedToWindow();
//  }

  @Override
  public void setOnViewAttachListener(OnViewAttachListener listener)
  {
    if (delegate == null) delegate = new HasOnViewAttachListenerDelegate(this);
    delegate.setOnViewAttachListener(listener);
  }

//  @Override
//  protected void onDetachedFromWindow()
//  {
//    super.onDetachedFromWindow();
//    delegate.onDetachedFromWindow();
//  }
}
