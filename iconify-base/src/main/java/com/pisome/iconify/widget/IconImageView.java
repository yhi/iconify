package com.pisome.iconify.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

import com.pisome.iconify.IconDrawable;
import com.pisome.iconify.R;
import com.pisome.iconify.internal.HasOnViewAttachListener;

public final class IconImageView extends ImageView implements HasOnViewAttachListener
{
  static final private String TAG = "IconImageView";
  private HasOnViewAttachListenerDelegate delegate;

  private IconDrawable icon;

  public IconImageView(final Context context)
  {
    this(context, null);
  }

  public IconImageView(final Context context, final AttributeSet attrs)
  {
    this(context, attrs, 0);
  }

  public IconImageView(final Context context, final AttributeSet attrs, final int defStyleAttr)
  {
    super(context, attrs, defStyleAttr);

    if (!this.isInEditMode())
    {
      this.init(context, attrs, defStyleAttr);
    }
  }

  private void init(final Context context, final AttributeSet attrs, final int defStyleAttr)
  {
    final TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.Iconify, defStyleAttr, 0);
    this.icon = IconDrawable.create(context, ta);
    this.setScaleType(ScaleType.CENTER_INSIDE);
    this.setImageDrawable(this.icon);
    ta.recycle();
  }

  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  public IconImageView(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes)
  {
    super(context, attrs, defStyleAttr, defStyleRes);

    if (this.isInEditMode())
    {
      this.init(context, attrs, defStyleAttr);
    }
  }

  public void setIcon(final String iconName)
  {
    this.icon.icon(iconName);
    this.setImageDrawable(this.icon);
  }

  @Override
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    delegate.onAttachedToWindow();
  }

  @Override
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    delegate.onDetachedFromWindow();
  }

  @Override
  public void setOnViewAttachListener(OnViewAttachListener listener)
  {
    Log.d(TAG, "[setOnViewAttachListener] listener = " + listener);
    if (delegate == null) delegate = new HasOnViewAttachListenerDelegate(this);
    delegate.setOnViewAttachListener(listener);
  }
}
