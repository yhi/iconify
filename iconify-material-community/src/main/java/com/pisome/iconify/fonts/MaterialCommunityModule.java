package com.pisome.iconify.fonts;

import com.pisome.iconify.Icon;
import com.pisome.iconify.IconFontDescriptor;

public class MaterialCommunityModule implements IconFontDescriptor {

    @Override
    public String ttfFileName() {
        return "iconify/android-iconify-material-community.ttf";
    }

    @Override
    public Icon[] characters() {
        return MaterialCommunityIcons.values();
    }

    @Override
    public String prefix()
    {
        return "mdi";
    }
}
